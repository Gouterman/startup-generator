import data from './data'

const rangeEls: NodeListOf<HTMLInputElement> = document.querySelectorAll('.range-input')
const generateBtnEl = document.getElementById('generate')
const randomBtnEl = document.getElementById('random')
const resultEl = document.getElementById('result')

const randomIntFromInterval = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

const displayResult = (name: string) => {
  if (name && resultEl) {
    resultEl!.textContent = name
  }
}

const find = (numbers: any) => {
  const startupName = []

  for (let index = 0; index < numbers.length; index++) {
    const number = numbers[index];

    if (data[index] && data[index].data[number]) {
      startupName.push(data[index].data[number])
    }
  }

  displayResult(startupName.join(' '))
}

// Listeners
generateBtnEl?.addEventListener('click', () => {
  const numbers = []

  for (const rangeEl of rangeEls) {
    numbers.push(parseInt(rangeEl.value))
  }

  find(numbers)
})

randomBtnEl?.addEventListener('click', () => {
  const numbers = []
  let index = 0

  for (const item of data) {
    const randomNumber = randomIntFromInterval(0, item.data.length - 1)
    numbers.push(randomNumber)

    if (rangeEls[index]) {
      rangeEls[index].value = randomNumber.toString()
    }

    index++
  }

  find(numbers)
})